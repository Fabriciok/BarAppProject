import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Menu } from './menu';


@NgModule({
  declarations: [
    Menu,
  ],
  imports: [
    
  ],
  exports: [
    Menu
  ]
})
export class MenuModule {}
