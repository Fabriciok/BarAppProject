import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';
import {BankApi} from '../../shared/barapp-api-service';
import {Carrinho} from '../../pages/carrinho/carrinho';
/**
 * Generated class for the Menu page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class Menu {

  clientes = [];
  clients : any;
  bebidas2 : any = [];
  lanches = [];
  sessionNames : any;
  doces : any = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private bankApi: BankApi) {
    this.clientes = navParams.data;
  }

  
    ionViewDidLoad() {
    
    this.clients = this.navParams.data;
    this.bankApi.getBebidas(this.clients.id).subscribe( data => {
      
      this.bebidas2 = data;
      
    });
    this.bankApi.getLanches(this.clients.id).subscribe( data => {
      this.lanches = data;
      
    });
    this.bankApi.getDoces(this.clients.id).subscribe( data => {
      this.doces = data;
      
    });
    
  }

  itemTapped(cliente) {
    
    this.navCtrl.push(Carrinho, cliente);
  }
  
  goHome()
  {
    this.navCtrl.popToRoot();
  }

  

  getItems(ev: any) {
    // Reset items back to all of the items
    

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.bebidas2 = this.bebidas2.filter((bebidas) => {
        return (bebidas.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

      this.lanches = this.lanches.filter((lanche) => {
        return (lanche.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

      this.doces = this.doces.filter((doce) => {
        return (doce.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })

      
    }else{
      this.ionViewDidLoad();
    }
  }
    
      
    


  



}
