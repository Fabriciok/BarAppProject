import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Payment } from './payment';

@NgModule({
  declarations: [
    Payment,
  ],
  imports: [
    
  ],
  exports: [
    Payment
  ]
})
export class PaymentModule {}
