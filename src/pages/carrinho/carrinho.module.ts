import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Carrinho } from './carrinho';

@NgModule({
  declarations: [
    Carrinho,
  ],
  imports: [
    
  ],
  exports: [
    Carrinho
  ]
})
export class CarrinhoModule {}
