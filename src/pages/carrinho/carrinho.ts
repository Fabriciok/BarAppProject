import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {BankApi} from '../../shared/barapp-api-service';
import {Payment} from '../../pages/payment/payment'
/**
 * Generated class for the Carrinho page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-carrinho',
  templateUrl: 'carrinho.html',
})
export class Carrinho {

  clients : any = [];
  bebidas = [];
  lanches = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private bankApi: BankApi) {
  }

  ionViewDidLoad() {
    this.clients = this.navParams.data;
    this.clients
    this.bankApi.getBebidas(this.clients.id).subscribe( data => {
      this.bebidas = data;
    });
    this.bankApi.getLanches(this.clients.id).subscribe( data => {
      this.lanches = data;
    }

    );
  }
  itemTapped()
  {
    this.navCtrl.push(Payment)
  }

  goHome()
  {
    this.navCtrl.popToRoot();
  }

}
