import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {BankApi} from '../../shared/barapp-api-service';
import {Menu} from '../../pages/menu/menu'
import * as _ from 'lodash';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  entireDatabase = [];
  clientes : any = [];
  constructor(public navCtrl: NavController, private bankApi: BankApi ,private loadingController : LoadingController,  public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectsPage');
    let loader = this.loadingController.create({
      content : "Getting content.."

    });

    loader.present().then(() => {

    
    this.bankApi.getEntireBarAppDatabase().subscribe( data => {
      this.entireDatabase = data;
      loader.dismiss();
    });
  });

  

}




itemTapped(clientes) {
   console.log(clientes);
    this.navCtrl.push(Menu, clientes);
  }

  getClientes(ev: any) {
    // Reset items back to all of the items
    

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.entireDatabase = this.entireDatabase.filter((clients) => {
        return (clients.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      
    }else{
      this.ionViewDidLoad();
    }
      
    


  }



}
