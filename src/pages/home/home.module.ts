import { NgModule, ErrorHandler } from '@angular/core';

import { IonicModule, IonicErrorHandler, IonicApp  } from 'ionic-angular';
import { HomePage } from './menu';
import { HttpModule } from '@angular/http';
import {BankApi} from '../shared/barapp-api-service';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    
  ],
  exports: [
    HomePage
  ],
  providers : [
    BankApi,
    HttpModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler, HttpModule}
  ]
})
export class HomeModule {}
