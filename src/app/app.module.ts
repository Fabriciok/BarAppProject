import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {Menu} from '../pages/menu/menu'
import {BankApi} from '../shared/barapp-api-service';
import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Carrinho} from '../pages/carrinho/carrinho';
import {Payment} from '../pages/payment/payment'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Menu,
    Carrinho,
    Payment
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Menu,
    Carrinho,
    Payment
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BankApi,
    HttpModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
