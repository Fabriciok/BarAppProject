import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {HttpModule} from '@angular/http'
import 'rxjs';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BankApi {
    private baseUrl = 'https://barappproject.firebaseio.com/';

    entireDatabase = [];
    nameBebidas : any = {};
    lanches : any = {};
    clients : any = {};
    doces : any = {};
    constructor(private http: Http) {

    }

    getEntireBarAppDatabase() : Observable<any>{
        return this.http.get(`${this.baseUrl}.json`)
            .map( (response : Response) => {
                this.entireDatabase = response.json();
                return this.entireDatabase;
            });

    }
    getLanches(clientID) : Observable<any>{
        return this.http.get(`${this.baseUrl}/${clientID}/lanches.json`)
        .map((response : Response) => {
            this.lanches = response.json();
            return this.lanches;
        });
    }

    getBebidas(clientID) : Observable<any>{
        return this.http.get(`${this.baseUrl}/${clientID}/bebidas.json`)
        .map((response : Response) => {
            this.nameBebidas = response.json();
            return this.nameBebidas;
        });
    }

    getDoces(clientID) : Observable<any>{
        return this.http.get(`${this.baseUrl}/${clientID}/doces.json`)
        .map((response : Response) => {
            this.doces = response.json();
            return this.doces;
        });
    }

    

}
